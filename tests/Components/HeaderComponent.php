<?php

namespace PRETests\Components;

use PRE\Component;
use PRE\Environment;

class HeaderComponent extends Component {

    public $title;

    public $entries = [];

    public function __construct(Environment $environment)
    {
        parent::__construct($environment);

        // Set default entries value.
        $this->entries = [
            [
                ['/docs/base', 'Alert'],
                ['/docs/base/button', 'Buttons'],
                ['/docs/base/icons', 'Icons'],
                ['/docs/base/table', 'Table'],
            ],
            [
                ['/docs/forms', 'Forms'],
                ['/docs/input', 'Input'],
                ['/docs/select', 'Select'],
            ],
        ];
    }

    public function render($variables = [])
    {
        return $this->env->render('./templates/header.xml', [
            'entries' => $this->entries,
            'title' => $this->title,
        ]);
    }

}
