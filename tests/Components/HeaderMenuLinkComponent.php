<?php

namespace PRETests\Components;

use PRE\Component;

class HeaderMenuLinkComponent extends Component {

    public $href;

    /**
     * {@inheritDoc}
     */
    public function render($variables = [])
    {
        return $this->env->render('./templates/header_menu_link.xml', [
            '_content' => $variables['_content'],
            'href' => $this->href,
        ]);
    }

}
