<?php

namespace PRETests\Components;

use PRE\Component;

class HeaderMenuItemComponent extends Component {

    /**
     * {@inheritDoc}
     */
    public function render($variables = [])
    {
        return $this->env->render('./templates/header_menu_item.xml', $variables);
    }
}
