<?php

namespace PRETests\Components;

use PRE\Component;

class HeaderLogoComponent extends Component {

    protected $dependencies = [
        'js' => [
            './test.js',
        ],
    ];

    public $href;

    public function render($variables = [])
    {
        return $this->env->render('./templates/header_logo.xml', [
            '_content' => $variables['_content'],
            'href' => $this->href,
        ]);
    }

}
