<?php

namespace PRETests\Components;

use PRE\Component;

class CursiveComponent extends Component {

    /**
     * {@inheritDoc}
     */
    public function render($variables = [])
    {
        return $this->env->render('./templates/cursive.xml', $variables);
    }

}
