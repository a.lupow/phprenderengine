<?php

require '../vendor/autoload.php';

$loader = new \PRE\Loaders\FileLoader();
$loader->addRootNamespace('PRETests\\Components');

$bundler = new \PRE\Bundlers\DefaultBundler();
$renderer = new \PRE\Environment($loader, $bundler, './cache');

$attributes = new \PRE\AttributesBundle([
    'class' => ['foo', 'bar'],
    'data-test' => ['foo' => 'bar'],
]);
$renderer->addGlobal('test', $attributes);

$start = microtime(true);
echo $renderer->render('templates/page.xml', [
    'bar' => [],
    'menu' => [
        [
            ['/', 'Foo'],
        ],
        [
            ['/', 'Bar'],
        ],
    ],
]);
$end = microtime(true);

echo "\n" . $end - $start;

var_dump($renderer->getBundler()->getDependencies());

//$renderer->getCache()->clearAll();
