<?php

namespace PRE\Bundlers;

use PRE\Component;
use PRE\ResourceBundlerInterface;

class DefaultBundler implements ResourceBundlerInterface {

    protected $dependencies = [];

    /**
     * {@inheritDoc}
     */
    public function addDependency(Component $component, $dependency, $type = NULL)
    {
        $this->dependencies[get_class($component)][$type][] = $dependency;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getDependencies($type = NULL)
    {
        if (is_null($type)) {
            return $this->dependencies;
        }

        if (isset($this->dependencies[$type])) {
            return $this->dependencies[$type];
        }

        return FALSE;
    }
}
