<?php

namespace PRE;

interface CacheTemplateInterface {

    /**
     * Renders out the template.
     *
     * @param $variables
     *   Template variables.
     *
     * @return string
     */
    public function render(array $variables = []): string;

}
