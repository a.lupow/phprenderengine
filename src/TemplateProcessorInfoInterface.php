<?php

namespace PRE;

interface TemplateProcessorInfoInterface {

    /**
     * Returns current processed template class.
     *
     * @return string
     */
    public function getTemplateClass(): string;

    /**
     * Returns current processed template content.
     *
     * @return string
     */
    public function getTemplateContent(): string;

}
