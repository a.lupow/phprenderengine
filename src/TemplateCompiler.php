<?php

namespace PRE;

use PRE\Nodes\RootNode;

class TemplateCompiler implements CompilerInterface {

    /**
     * Current environment.
     *
     * @var Environment
     */
    protected $env;

    /**
     * Current source stack.
     *
     * @var string[]
     */
    protected $source;

    /**
     * Current buffer pointer.
     *
     * @var int
     */
    protected $buffer;

    /**
     * Top buffer depth.
     *
     * @var int
     */
    protected $bufferMax;

    /**
     * TemplateCompiler constructor.
     *
     * @param Environment $environment
     */
    public function __construct(Environment $environment)
    {
        $this->env = $environment;
        $this->reset();
    }

    /**
     * Resets compiler source.
     *
     * @return $this
     */
    public function reset() {
        $this->source = [];
        $this->buffer = 0;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function addLine($line) {
        $this->source[] = $line;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function fork()
    {
        $this->buffer++;
        $this->bufferMax = max($this->bufferMax, $this->buffer);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function unfork()
    {
        $this->buffer = max(0, --$this->buffer);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getBuffer()
    {
        return $this->buffer;
    }

    /**
     * {@inheritDoc}
     */
    public function assignToBuffer($expression, $buffer = NULL)
    {
        if ($buffer === NULL) {
            $buffer = $this->buffer;
        }

        $line = "\$buffer_$buffer = $expression";

        return $this->addLine($line);
    }

    /**
     * {@inheritDoc}
     */
    public function concatBuffer($expression, $buffer = NULL)
    {
        if ($buffer === NULL) {
            $buffer = $this->buffer;
        }

        $line = "\$buffer_$buffer .= $expression";

        return $this->addLine($line);
    }

    /**
     * {@inheritDoc}
     */
    public function compile(Template $template): TemplateProcessorInfoInterface {
        // Parse the file first.
        $root = $this->env->getParser()->parse($template);

        // Build source lines.
        $root->compile($this);

        // Include a buffer stack initalisation statement.
        $top_buffer = $this->bufferMax + 1;
        array_unshift($this->source, "\$this->componentChildren = array_fill(0, $top_buffer, [])");

        // Write out a test cache class.
        $class = $template->getCacheId();
        $writer = new ClassWriter($class, CacheTemplate::class);
        $writer->addMethodReflect(CacheTemplate::class, 'render', implode(";\n", $this->source) . ';');

        // Reset the compiler.
        $this->reset();

        return new TemplateProcessorInfo($class, $writer->ouput());
    }

    /**
     * {@inheritDoc}
     */
    public function getEnvironment(): Environment
    {
        return $this->env;
    }

}
