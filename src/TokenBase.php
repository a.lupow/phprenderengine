<?php

namespace PRE;

use PRE\Exceptions\ParserException;

abstract class TokenBase {

    protected $content;

    public static abstract function getTokenStart();

    public static abstract function getTokenEnd();

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content = trim($content);
    }

    public abstract function handle(Stack $stack);

}
