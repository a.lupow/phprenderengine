<?php

namespace PRE;

use PRE\Exceptions\PRException;
use PRE\Nodes\RootNode;

interface ParserInterface {

    /**
     * Parse a target template.
     *
     * @param Template $template
     *   A template to parse.
     *
     * @return RootNode
     *   Root node.
     *
     * @throws PRException
     *   An exception is thrown whenever parsing was failed.
     */
    public function parse(Template $template): RootNode;

}
