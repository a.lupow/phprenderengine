<?php

namespace PRE;

abstract class CompilerNode {

    /**
     * Child nodes.
     *
     * @var CompilerNode[]
     */
    protected $children = [];

    /**
     * Add a child node.
     *
     * @param CompilerNode $node
     *   Target node.
     *
     * @return $this
     */
    public function addChild(CompilerNode $node) {
        $this->children[] = $node;

        return $this;
    }

    /**
     * Include multiple child nodes.
     *
     * @param CompilerNode[] $nodes
     *   Target nodes.
     *
     * @return $this
     */
    public function addChildren(array $nodes) {
        foreach ($nodes as $node) {
            $this->addChild($node);
        }

        return $this;
    }

    /**
     * Returns a set of compiler node children.
     *
     * @return CompilerNode[]
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Compiles the node.
     *
     * @param CompilerInterface $compiler
     *
     * @return mixed
     */
    public abstract function compile(CompilerInterface $compiler);

}
