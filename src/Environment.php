<?php

namespace PRE;

use Composer\Autoload\ClassLoader;
use PRE\Cache\TemplateCache;
use PRE\Exceptions\PRException;
use PRE\Exceptions\RuntimeException;

class Environment {

    /**
     * Environment loader.
     *
     * @var ComponentLoaderInterface
     */
    protected $loader;

    /**
     * Cache source.
     *
     * @var CacheInterface
     */
    protected $cache;

    /**
     * An array of initialised templates.
     *
     * @var array
     */
    protected $templates;

    /**
     * Template processor.
     *
     * @var CompilerInterface
     */
    protected $processor;

    /**
     * @var TemplateParser
     */
    protected $parser;

    /**
     * A set of environment global variables.
     *
     * @var array[]
     */
    protected $globalVariables;

    /**
     * Resource bundler.
     *
     * @var ResourceBundlerInterface
     */
    protected $bundler;

    protected $components;

    public function __construct(ComponentLoaderInterface $loader, ResourceBundlerInterface $bundler, $cache)
    {
        $this->loader = $loader;

        // Process cache options.
        $this->setCache($cache);

        // Initialise the processor.
        $this->processor = new TemplateCompiler($this);
        $this->parser = new TemplateParser($this);
        $this->bundler = $bundler;

        $this->templates = [];
        $this->clearGlobals();
    }

    /**
     * Adds a variable to the global variable collection.
     *
     * @param string $name
     *   A variable name.
     * @param $value
     *   A variable value.
     *
     * @return $this
     */
    public function addGlobal($name, $value) {
        $this->globalVariables[$name] = $value;

        return $this;
    }

    /**
     * Returns a set of global variables.
     *
     * @return array[]
     */
    public function getGlobals() {
        return $this->globalVariables;
    }

    /**
     * Clears out global variables storage.
     *
     * @return $this
     */
    public function clearGlobals() {
        $this->globalVariables = [];

        return $this;
    }

    /**
     * Creates a component instance with specified attributes.
     *
     * @param string $component
     *   Component identifier.
     * @param array $attributes
     *   An array of component attributes.
     *
     * @return Component
     *   Initialised component.
     */
    public function createComponent($component, array $attributes = []) {
        $class = $this->getLoader()->findClass($component);
        if (!class_exists($class)) {
            throw new RuntimeException(printf('Component not found: %s.', $component));
        }

        $instance = new $class($this);
        $instance->setAttributes($attributes);

        return $instance;
    }

    /**
     * Performs a template rendering.
     *
     * @param $filepath
     *   Target template.
     * @param array $variables
     *   Template variables.
     *
     * @return string
     */
    public function render($filepath, $variables = []) {
        return $this->loadTemplate($filepath)->render($variables);
    }

    /**
     * Compiles a specific template file.
     *
     * @param $filepath
     *   Target template path.
     *
     * @return TemplateProcessorInfoInterface
     */
    public function compile($filepath) {
        return $this->getProcessor()->compile(
            $this->loadTemplate($filepath)
        );
    }

    /**
     * Initialises a template.
     *
     * @param $filepath
     *   Target template.
     *
     * @return Template
     *   Initialised template.
     */
    public function loadTemplate($filepath): Template {
        if (isset($this->templates[$filepath]) && $this->templates[$filepath] instanceof Template) {
            return $this->templates[$filepath];
        }

        if (!file_exists($filepath)) {
            throw new PRException(printf('File does not exist %s.', $filepath));
        }

        return $this->templates[$filepath] = new Template($this, $filepath);
    }

    /**
     * Sets current environment cache.
     *
     * @param string|CacheInterface $cache
     *   Either cache directory or a cache interface instance.
     *
     * @return $this
     *
     * @throws PRException
     */
    public function setCache($cache) {
        if (is_string($cache)) {
            $cache = new TemplateCache($this, $cache);
        }

        if (!$cache instanceof CacheInterface) {
            throw new PRException('Unknown cache option.');
        }

        $this->cache = $cache;

        return $this;
    }

    /**
     * Finds a component class path.
     *
     * @param Component $component
     *   Target component.
     *
     * @return false|string
     *   File path or false if a class is a virtual one.
     */
    public function getComponentPath(Component $component) {
        return $this->getLoader()->findFileClass(get_class($component));
    }

    /**
     * Returns current environment cache source.
     *
     * @return CacheInterface
     */
    public function getCache() {
        return $this->cache;
    }

    /**
     * Returns current environment processor.
     *
     * @return CompilerInterface
     */
    public function getProcessor() {
        return $this->processor;
    }

    /**
     * Returns current environment parser.
     *
     * @return ParserInterface
     */
    public function getParser() {
        return $this->parser;
    }

    /**
     * Returns current environment loader.
     *
     * @return ComponentLoaderInterface
     */
    public function getLoader() {
        return $this->loader;
    }

    /**
     * Returns current resource bundler.
     *
     * @return ResourceBundlerInterface
     */
    public function getBundler() {
        return $this->bundler;
    }

    /**
     * Sets current environment bundler.
     *
     * @param ResourceBundlerInterface $bundler
     *   A bundler to be used.
     *
     * @return $this
     */
    public function setBundler(ResourceBundlerInterface $bundler) {
        $this->bundler = $bundler;

        return $this;
    }


}
