<?php

namespace PRE;

/**
 * Implements basic template cache class.
 */
abstract class CacheTemplate implements CacheTemplateInterface {

    /**
     * Stores a set of per level initialised components.
     *
     * @var Component[][]
     */
    protected $componentChildren;

    /**
     * Current rendering environment.
     *
     * @var Environment
     */
    protected $env;

    /**
     * CacheTemplate constructor.
     *
     * @param Environment $environment
     */
    public function __construct(Environment $environment)
    {
        $this->env = $environment;
        $this->componentChildren = [];
    }

    /**
     * Safely returns a variable from a source array.
     *
     * @param $token
     *   Variable token.
     * @param $source
     *   Variables source.
     *
     * @return mixed
     *   Variable value.
     */
    public function getVar($token, $source) {
        $value = '';

        // Extract values both from objects and arrays.
        if (is_object($source)) {
            if (property_exists($source, $token)) {
                // TODO: Property availability checks raising runtime
                //  exceptions.
                $value = $source->{$token};
            }
        }
        elseif (isset($source[$token])) {
            $value = $source[$token];
        }

        return $value;
    }

    /**
     * Executes a source object method.
     *
     * @param $method
     *   A method to be executed.
     * @param $arguments
     *   A set of method arguments
     * @param $source
     *   A source object.
     *
     * @return mixed
     *   Method result.
     */
    public function executeVar($method, $arguments, $source) {
        if (method_exists($source, $method)) {
            return $source->{$method}(...$arguments);
        }

        trigger_error(
            printf('Unknown method used: %s.', $method),
            E_NOTICE
        );

        return $source;
    }

    /**
     * Safely returns a variable as an iterator for a loop.
     *
     * @param $token
     *   Variable token.
     * @param $source
     *   Variables source.
     *
     * @return array
     */
    public function getIterator($token, $source) {
        $value = $this->getVar($token, $source);
        if (!is_iterable($value)) {
            $value = [];
        }

        return $value;
    }

    /**
     * Initialises a given component class.
     *
     * @param string $component_class
     *   Target component class.
     * @param int $buffer
     *   Current buffer level.
     *
     * @return Component
     */
    public function initialiseComponent($component_class, $buffer) {
        $component = new $component_class($this->env);
        $component->setChildren($this->componentChildren[$buffer]);

        // Keep track of components on each level.
        $this->componentChildren[$buffer - 1][] = $component;

        return $component;
    }

}
