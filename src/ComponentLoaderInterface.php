<?php

namespace PRE;

interface ComponentLoaderInterface {

    /**
     * Returns a class for a specific component.
     *
     * @param string $component
     *   The component identifier.
     *
     * @return string
     *   The component class.
     */
    public function findClass($component): string;

    /**
     * Finds a class file for a given component.
     *
     * @param $component
     *   Target component.
     *
     * @return string
     */
    public function findFile($component): string;

    /**
     * Returns a class file path.
     *
     * @param $class
     *   Target class.
     *
     * @return string
     *   Class definition file path.
     */
    public function findFileClass($class): string;

}
