<?php

namespace PRE;

interface CacheInterface {

    /**
     * Returns processed template class if such exists.
     *
     * @param string $filename
     *   Target template.
     *
     * @return string|bool
     *   Target class or FALSE if not found.
     */
    public function getCache($filename);

    /**
     * Stores processed template.
     *
     * @param $filename
     *   Target template.
     * @param $content
     *   Template processed content.
     *
     * @return array
     *   Newly created cache record.
     */
    public function setCache($filename, $content = NULL);

    /**
     * Clears out the cache.
     *
     * @return $this
     */
    public function clearAll();

}
