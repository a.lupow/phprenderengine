<?php

namespace PRE;

class ClassWriter {

    protected $class;

    protected $extends;
    protected $implements;
    protected $dependencies;

    protected $methods;

    public function __construct($class, $extends = NULL, $implements = [])
    {
        $this->class = $class;
        $this->extends = $extends;
        $this->implements = $implements;

        $this->methods = [];
        $this->dependencies = [];
    }

    /**
     * Reflects an arbitrary class method with arbitrary content.
     *
     * @param string $class
     *   Target class.
     * @param $method
     *   Target class method.
     * @param $content
     *   Method content.
     *
     * @return $this
     *
     * @throws \ReflectionException
     */
    public function addMethodReflect($class, $method, $content) {
        $reflection = new \ReflectionClass($class);

        // Include an interface in the implementation set.
        if ($reflection->isInterface() && !in_array($reflection->getShortName(), $this->implements)) {
            $this->implements[] = $reflection->getShortName();
        }

        $reflection_method = $reflection->getMethod($method);
        $modifiers = $reflection_method->getModifiers();
        $return_type = NULL;
        if ($reflection_method->hasReturnType()) {
            $return_type = (string) $reflection_method->getReturnType();
        }

        // Detect the method type.
        $type = 'public';
        if ($modifiers & \ReflectionMethod::IS_PRIVATE) {
            $type = 'private';
        }

        // Process the arguments.
        $arguments = [];
        foreach ($reflection_method->getParameters() as $parameter) {
            $argument = "$$parameter->name";

            // Prepend an argument type if necessary.
            if ($parameter->hasType()) {
                $argument_type = $parameter->getType();

                if (!$argument_type->isBuiltin()) {
                    $argument_type = new \ReflectionClass((string) $argument_type);
                    $this->addDependency($argument_type->name);

                    $argument_type = $argument_type->getShortName();
                }

                $argument = (string) $argument_type . ' ' . $argument;
            }

            if ($parameter->isOptional()) {
                $argument_default = $parameter->getDefaultValue();
                if (is_array($argument_default)) {
                    $argument_default = '[' . implode(', ', $argument_default) . ']';
                }
                else {
                    $argument_default = (string) $argument_default;
                }

                $argument .= " = {$argument_default}";
            }

            $arguments[] = $argument;
        }

        return $this->addMethod(
            $reflection_method->getName(),
            $content,
            $type,
            $arguments,
            $return_type
        );
    }

    /**
     * Stacks a new method.
     *
     * @param $name
     *   Method name.
     * @param $content
     *   Method content.
     * @param string $type
     *   Method accessor.
     * @param array $arguments
     *   Method arguments.
     * @param bool $return_type
     *   Method return type.
     *
     * @return $this
     */
    public function addMethod($name, $content, $type = 'public', $arguments = [], $return_type = FALSE) {
        $this->methods[$name] = [
            'type' => $type,
            'content' => $content,
            'arguments' => $arguments,
            'return_type' => $return_type,
        ];

        return $this;
    }

    /**
     * Includes a class dependency to be added to the use statements.
     *
     * @param $class
     *   Target class.
     *
     * @return $this
     */
    public function addDependency($class) {
        if (!in_array($class, $this->dependencies)) {
            $this->dependencies[] = $class;
        }

        return $this;
    }

    /**
     * Render out the class.
     *
     * @return string
     */
    public function ouput() {
        $class = "<?php\n";

        // Render out use statements.
        foreach ($this->dependencies as $dependency) {
            $class .= "use {$dependency};\n";
        }

        // Render out the header.
        $class .= "class {$this->class} ";

        if ($this->extends) {
            $class .= "extends {$this->extends} ";
        }

        if ($this->implements) {
            $implements = implode(', ', $this->implements);
            $class .= "implements {$implements} ";
        }

        $class .= "{\n";

        // Render out the methods.
        foreach ($this->methods as $method_name => $method) {
            $method_type = $method['type'];
            $method_content = $method['content'];
            $method_arguments = implode(', ', $method['arguments']);
            $method_return = $method['return_type'];

            $class .= "{$method_type} function {$method_name} ($method_arguments)";
            if ($method_return) {
                $class .= ": {$method_return}";
            }

            $class .= "{\n{$method_content}\n}\n";
        }

        $class .= "}\n";

        return $class;
    }

    public function write($path) {
        return file_put_contents($path, $this->ouput());
    }

}
