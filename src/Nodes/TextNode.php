<?php

namespace PRE\Nodes;

use PRE\CompilerInterface;
use PRE\CompilerNode;

class TextNode extends CompilerNode {

    protected $content;

    public function __construct($content)
    {
        $this->content = $content;
    }

    public function getContent() {
        return $this->content;
    }

    /**
     * {@inheritDoc}
     */
    public function compile(CompilerInterface $compiler)
    {
        // Escape string characters.
        $content = $this->getContent();
        $content = str_replace("'", "\'", $content);

        $compiler->concatBuffer("'$content'");
    }

}
