<?php

namespace PRE\Nodes;

use PRE\CompilerInterface;
use PRE\CompilerNode;
use PRE\Exceptions\ParserException;

class LoopNode extends CompilerNode {

    protected $expression;

    public function __construct($expression)
    {
        $this->expression = trim($expression);
    }

    /**
     * {@inheritDoc}
     */
    public function compile(CompilerInterface $compiler)
    {
        // Parse the expression.
        // For now, expect A in B formula.
        $operation = explode(' ', $this->expression);
        if (count($operation) < 3) {
            throw new ParserException('Unknown expression used in a loop.');
        }

        list($variable_local, $operation, $variable_source) = $operation;
        if ($operation !== 'in') {
            throw new ParserException(printf('Unknown operation "%s", expected "in".', $operation));
        }

        // Compile the initial point.
        $compiler->addLine(
            "foreach (\$this->getIterator('$variable_source', \$variables) as \$$variable_local) {"
        );

        // Set variable first.
        $compiler->addLine(
            "\$variables['$variable_local'] = \$$variable_local"
        );

        // Compile the contents.
        foreach ($this->getChildren() as $child) {
            $child->compile($compiler);
        }

        // Enclose the block.
        $compiler->addLine('}');
    }

}
