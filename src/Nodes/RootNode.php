<?php

namespace PRE\Nodes;

use PRE\CompilerInterface;
use PRE\CompilerNode;

class RootNode extends CompilerNode {

    /**
     * {@inheritDoc}
     */
    public function compile(CompilerInterface $compiler)
    {
        // Initialise global variables.
        $compiler->addLine(
            '$variables = array_merge($this->env->getGlobals(), $variables)'
        );

        // Initialise the buffer.
        $compiler->assignToBuffer('""');

        foreach ($this->getChildren() as $child) {
            $child->compile($compiler);
        }

        // Include zero buffer return statement.
        $compiler->addLine('return $buffer_0');
    }

}
