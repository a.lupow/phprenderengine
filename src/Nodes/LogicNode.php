<?php

namespace PRE\Nodes;

use PRE\CompilerInterface;
use PRE\CompilerNode;
use PRE\Exceptions\CompilerException;
use PRE\Exceptions\LogicException;

class LogicNode extends CompilerNode {

    public static $STATE_DEFAULT = 0;
    public static $STATE_ELSE = 1;

    /**
     * Logic expression to be evaluated.
     *
     * @var string
     */
    protected $expression;

    /**
     * Children for else body.
     *
     * @var CompilerNode[]
     */
    protected $children_else;

    /**
     * Current logic node state.
     * Used on parsing stage.
     *
     * @var integer
     */
    protected $state;

    /**
     * LogicNode constructor.
     *
     * @param $expression
     */
    public function __construct($expression)
    {
        $this->expression = trim($expression);
        $this->setState(static::$STATE_DEFAULT);
    }

    /**
     * Sets current logic node state.
     *
     * @param integer $state
     *   State value.
     *
     * @return $this
     */
    public function setState($state) {
        $this->state = $state;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function addChild(CompilerNode $node)
    {
        switch ($this->state) {
            // Fire the default behaviour on the default state.
            case static::$STATE_DEFAULT:
                parent::addChild($node);

                break;

            // Append to the else section otherwise.
            case static::$STATE_ELSE:
                $this->children_else[] = $node;

                break;

            default:
                throw new LogicException('Unknown logic state.');
        }

        return $this;
    }

    /**
     * Extracts operation out of current expression.
     *
     * @return array
     */
    protected function extractOperation() {
        $operation = array_reverse(explode(' ', $this->expression));

        if (count($operation) < 3) {
            $operation = array_merge($operation, array_fill(0, 3, false));
        }

        return $operation;
    }

    /**
     * Compiles an expression associated with the node.
     *
     * @return string
     *   Resulted php expression.
     *
     * @throws CompilerException
     */
    public function compileExpression() {
        list ($operand_right, $operation, $operand_left) = $this->extractOperation();
        $expression = "\$this->getVar('$operand_right', \$variables)";

        // TODO: Implement a correct expression converter.
        if ($operation) {
            switch ($operation) {
                case 'not':
                    $expression = '!' . $expression;

                    break;

                default:
                    throw new CompilerException(printf('Unknown operation "%s".', $operation));
            }
        }

        return $expression;
    }

    /**
     * @inheritDoc
     */
    public function compile(CompilerInterface $compiler)
    {
        // Evaluate the expression.
        $expression = $this->compileExpression();
        $compiler->addLine(
            "if ($expression) {"
        );

        // Compile default children next.
        foreach ($this->children as $child) {
            $child->compile($compiler);
        }
        
        if ($this->children_else) {
            $compiler->addLine(
                '} else {'
            );
            
            foreach ($this->children_else as $child) {
                $child->compile($compiler);
            }
        }

        $compiler->addLine('}');
    }

}
