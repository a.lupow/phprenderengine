<?php

namespace PRE\Nodes;

use PRE\CompilerInterface;
use PRE\CompilerNode;
use PRE\Exceptions\CompilerException;

class ComponentNode extends CompilerNode {

    protected $id;

    protected $attributes;

    public function __construct($id, array $attributes = [])
    {
        $this->id = $id;
        $this->attributes = $attributes;
    }

    public function getId() {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function compile(CompilerInterface $compiler)
    {
        // Initialise the component first finding the target class.
        $class = $compiler->getEnvironment()->getLoader()->findClass($this->id);
        if (!$class) {
            throw new CompilerException(printf('Missing component: %s.', $this->id));
        }

        // Render children in a separate buffer.
        $compiler->fork();
        $buffer = $compiler->getBuffer();
        $compiler->assignToBuffer('""');

        // Compile child elements.
        foreach ($this->getChildren() as $child) {
            $child->compile($compiler);
        }

        $attributes = [];
        foreach ($this->attributes as $attribute_name => $attribute) {
            // Supports variables and text nodes.
            // TODO: Come up w/ a better solution.
            if ($attribute instanceof VariableNode) {
                $attribute_value = $attribute->compileExpression();
            }
            elseif ($attribute instanceof TextNode) {
                $content = $attribute->getContent();
                $attribute_value = "'$content'";
            }

            $attributes[] = "'$attribute_name' => $attribute_value";
        }

        $attributes = '[' . join(', ', $attributes) . ']';
        $compiler->unfork()->concatBuffer(
            "\$this->initialiseComponent('$class', $buffer)->setAttributes($attributes)->render(['_content' => \$buffer_$buffer])"
        );
    }

}
