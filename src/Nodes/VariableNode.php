<?php

namespace PRE\Nodes;

use PRE\CompilerInterface;
use PRE\CompilerNode;

class VariableNode extends CompilerNode {

    protected $expression;

    public function __construct($expression)
    {
        $this->expression = trim($expression);
    }

    public function compileExpression() {
        // Detect accessors.
        $accessors = explode('.', $this->expression);

        // Compile getVar methods.
        $source = '$variables';
        foreach ($accessors as $accessor) {
            // Detect method executions.
            if (substr($accessor, -1) === ')') {
                $method = substr($accessor, 0, strpos($accessor, '('));
                $arguments = substr($accessor, strlen($method) + 1, -1);

                $source = "\$this->executeVar('$method', [$arguments], $source)";
            }
            // Simply extract variable otherwise.
            else {
                $source = "\$this->getVar('$accessor', $source)";
            }
        }

        return $source;
    }

    /**
     * @inheritDoc
     */
    public function compile(CompilerInterface $compiler)
    {
        // TODO: Add support for complex expressions.
        $compiler->concatBuffer($this->compileExpression());
    }

}
