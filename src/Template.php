<?php

namespace PRE;

use PRE\Exceptions\PRException;
use function Composer\Autoload\includeFile;

class Template {

    /**
     * Rendering environment.
     *
     * @var Environment
     */
    protected $env;

    /**
     * Template filepath.
     *
     * @var string
     */
    protected $filepath;

    protected $cacheId;

    public function __construct(Environment $environment, $filepath)
    {
        $this->env = $environment;
        $this->filepath = $filepath;
    }

    public function getPath() {
        return $this->filepath;
    }

    public function getCacheId() {
        if ($this->cacheId) {
            return $this->cacheId;
        }

        $filepath_hash = md5($this->filepath);
        $filepath_time = filemtime($this->getPath());

        $class = '__Template_' . $filepath_hash . '_' . $filepath_time;

        return $this->cacheId = $class;
    }

    protected function initCache($cache) {
        $class = $cache['class'];

        if (!class_exists($class)) {
            include $cache['path'];
        }

        $instance = new $class($this->env);
        if (!$instance instanceof CacheTemplateInterface) {
            throw new PRException('Unknown cache template class.');
        }

        return $instance;
    }

    public function render($variables = []): string {
        $cache = $this->env->getCache()->getCache($this->filepath);

        // If cache is not available, process the file.
        if (!$cache) {
            $compilation = $this->env->compile($this->filepath);
            $cache_storage = $this->env->getCache();

            $cache = $cache_storage->setCache(
                $this->filepath, $compilation->getTemplateContent()
            );
        }

        // Render out the processed template.
        $processed_template = $this->initCache($cache);

        return $processed_template->render($variables);
    }

}
