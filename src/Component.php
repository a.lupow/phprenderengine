<?php

namespace PRE;

use PRE\Exceptions\RuntimeException;

abstract class Component {

    /**
     * Rendering environment.
     *
     * @var Environment
     */
    protected $env;

    /**
     * An array of component dependencies.
     *
     * @var string[][]
     */
    protected $dependencies = [];

    /**
     * A set of subcomponents.
     *
     * @var Component[]
     */
    protected $children = [];

    /**
     * Component constructor.
     *
     * @param Environment $environment
     *   Current rendering environment.
     */
    public function __construct(Environment $environment)
    {
        $this->env = $environment;

        // Register dependencies on component initialisation.
        // TODO: Make compiler call the method once.
        $this->registerDependencies();
    }

    /**
     * Registers a set of dependencies defined for the component.
     *
     * @return $this
     */
    public function registerDependencies() {
        if (empty($this->dependencies)) {
            return $this;
        }

        foreach ($this->dependencies as $dependency_type => $entries) {
            foreach ($entries as $entry) {
                $this->addDependency($entry, $dependency_type);
            }
        }

        // To prevent the dependencies from being added twice,
        // empty the array.
        // @see a to do mark above.
        $this->dependencies = [];

        return $this;
    }

    /**
     * Returns current component path.
     *
     * @return false|string
     *   File path or false if none exists.
     */
    public function getPath() {
        return $this->env->getComponentPath($this);
    }

    /**
     * Registers a dependency of a specific type.
     *
     * @param $value
     *   Dependency value.
     * @param null $type
     *   Dependency type.
     *
     * @return $this
     */
    protected function addDependency($value, $type = NULL) {
        $this->env->getBundler()->addDependency($this, $value, $type);

        return $this;
    }

    /**
     * Sets attributes for the component.
     *
     * @param array $attributes
     *   An array of attributes.
     *
     * @return $this
     */
    public function setAttributes($attributes = []) {
        foreach ($attributes as $attribute => $value) {
            if (property_exists(Component::class, $attribute)) {
                throw new RuntimeException('Attempted to override internal components attribute.');
            }
            elseif (property_exists($this, $attribute)) {
                $this->{$attribute} = $value;
            }
            else {
                trigger_error(
                    printf('An unknown attribute "%s" specified for component "%s".', $attribute, static::class),
                    E_USER_NOTICE
                );
            }
        }

        return $this;
    }

    /**
     * Sets a set of child elements for the component.
     *
     * @param Component[] $children
     *   A set of child components.
     *
     * @return $this
     */
    public final function setChildren(array $children = []) {
        $this->children = $children;

        return $this;
    }

    /**
     * Returns a set of child components.
     *
     * @return Component[]
     *   A set of child components.
     */
    public final function getChildren() {
        return $this->children;
    }

    /**
     * Renders out the component.
     *
     * @param array $variables
     *   A set of input variables.
     *
     * @return string
     *   Rendered out component.
     */
    abstract public function render($variables = []);

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {
        return $this->render([
            '_content' => '',
        ]);
    }

}
