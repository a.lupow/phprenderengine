<?php

namespace PRE;

interface ResourceBundlerInterface {

    /**
     * Include dependency in the bundle.
     *
     * @param Component $component
     *   A component that requires the dependency.
     * @param $dependency
     *   Component dependency.
     * @param string $type
     *   Optional dependency type.
     *
     * @return $this
     */
    public function addDependency(Component $component, $dependency, $type = NULL);

    /**
     * Returns a set of bundle dependencies.
     *
     * @param string $type
     *   Dependencies type. If null, will return the complete set.
     *
     * @return array
     */
    public function getDependencies($type = NULL);

}
