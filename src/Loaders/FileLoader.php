<?php

namespace PRE\Loaders;

use PRE\ComponentLoaderInterface;

class FileLoader implements ComponentLoaderInterface {

    protected $namespaces = [];

    protected $map = [];

    protected $mapFiles = [];

    protected $mapClass = [];

    /**
     * Includes a root namespace in the list of namespaces to search for.
     *
     * @param string $namespace
     *   Target root namespace.
     *
     * @return $this
     */
    public function addRootNamespace($namespace) {
        $this->namespaces[] = $namespace;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function findClass($component): string
    {
        if (isset($this->map[$component])) {
            return $this->map[$component];
        }

        $component_name = ucfirst($component);
        $component_class = FALSE;

        foreach ($this->namespaces as $namespace) {
            // Scan directories first.
            $classes = [
                $namespace . '\\' . $component_name . '\\' . $component_name . 'Component',
                $namespace . '\\' . $component_name . 'Component',
            ];

            foreach ($classes as $class) {
                if (class_exists($class)) {
                    $component_class = $class;

                    break 2;
                }
            }
        }

        return $this->map[$component] = $component_class;
    }

    /**
     * {@inheritDoc}
     */
    public function findFileClass($class): string
    {
        if (isset($this->mapClass[$class])) {
            return $this->mapClass[$class];
        }

        $reflection = new \ReflectionClass($class);
        return $this->mapClass[$class] = $reflection->getFileName();
    }

    /**
     * {@inheritDoc}
     */
    public function findFile($component): string
    {
        if (isset($this->mapFiles[$component])) {
            return $this->mapFiles[$component];
        }

        $class = $this->findClass($component);
        if (!$class) {
            return FALSE;
        }

        return $this->mapFiles[$component] = $this->findFileClass($class);
    }

}
