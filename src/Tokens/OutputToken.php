<?php

namespace PRE\Tokens;

use PRE\Nodes\VariableNode;
use PRE\Stack;
use PRE\TokenBase;

class OutputToken extends TokenBase {

    public static function getTokenStart()
    {
        return '{{';
    }

    public static function getTokenEnd()
    {
        return '}}';
    }

    public function handle(Stack $stack)
    {
        // That token cannot hold any children, simply add a node.
        $stack->current()->addChild(
            new VariableNode($this->content)
        );
    }

}
