<?php

namespace PRE\Tokens;

use PRE\Exceptions\ParserException;
use PRE\Nodes\ComponentNode;
use PRE\Nodes\TextNode;
use PRE\Nodes\VariableNode;
use PRE\Stack;
use PRE\TokenBase;

class ComponentToken extends TokenBase {

    public static function getTokenStart()
    {
        return '<pre-';
    }

    public static function getTokenEnd()
    {
        return '>';
    }

    public function handle(Stack $stack)
    {
        // Create a component node and push it to the stack, appending to the current one.
        // Parse the content.
        $separator = strpos($this->content, ' ');
        if ($separator === FALSE) {
            $id = $this->content;
        }
        else {
            $id = substr($this->content, 0, $separator);
        }

        $attributes = [];
        $attributes_text = substr($this->content, strlen($id) + 1);
        $attributes_match = [];

        preg_match_all('/(\w+)=("|{{2})(.*?)("|}{2})/', $attributes_text, $attributes_match);
        if ($attributes_match) {
            foreach ($attributes_match[0] as $index => $match) {
                $attribute = NULL;

                switch ($attributes_match[2][$index]) {
                    case '"':
                        // Make sure it's a correct attribute definition.
                        if ($attributes_match[2][$index] !== $attributes_match[4][$index]) {
                            throw new ParserException('Unexpected token.');
                        }

                        $attribute = new TextNode($attributes_match[3][$index]);

                        break;

                    case '{{':
                        // Make sure it has correct enclosing.
                        if ($attributes_match[4][$index] !== '}}') {
                            throw new ParserException('Incorrect component attribute expression used.');
                        }

                        $attribute = new VariableNode($attributes_match[3][$index]);

                        break;

                    default:
                        throw new ParserException('Unknown component attribute opening token.');
                }

                // Store the attribute.
                $attributes[$attributes_match[1][$index]] = $attribute;
            }
        }

        // Create a new component node.
        $node = new ComponentNode($id, $attributes);

        // Append it to the current one and push it to the stack.
        $stack->current()->addChild($node);

        // Check whether the token is a self closing one.
        // If it is, do not stack the node.
        // Not sure whether that is needed, perhaps it's better
        // to let a node decide whether it allows content or not
        // to be specified (similar to an img element).
        if (substr($this->content, -1) !== '/') {
            $stack->push($node);
        }
    }

}
