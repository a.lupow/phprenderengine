<?php

namespace PRE\Tokens;

use PRE\Exceptions\ParserException;
use PRE\Nodes\LogicNode;
use PRE\Nodes\LoopNode;
use PRE\Stack;
use PRE\TokenBase;

class LogicToken extends TokenBase {

    /**
     * {@inheritDoc}
     */
    public static function getTokenStart()
    {
        return '{%';
    }

    /**
     * {@inheritDoc}
     */
    public static function getTokenEnd()
    {
        return '%}';
    }

    /**
     * Returns a set of potential nodes.
     *
     * @return string[]
     */
    protected function getLogicNodes() {
        return [
            'if' => LogicNode::class,
            'for' => LoopNode::class,
        ];
    }

    protected function getOpeningWords() {
        return array_keys($this->getLogicNodes());
    }

    public function handle(Stack $stack)
    {
        // Make a decision based on the token type.
        $words = array_filter(explode(' ', $this->content));
        $current = $stack->current();

        if (!count($words)) {
            throw new ParserException('Empty logic tag found.');
        }

        // If a logic node is not the current node, treat the token as an opening.
        // One word statements are being treated as enclosing statements.
        if (count($words) === 1) {
            if ($current instanceof LogicNode) {
                // Switch current logic node state based on the keyword.
                // Expecting only one.
                switch ($words[0]) {
                    // Change logic branch state.
                    case 'else':
                        $current->setState(
                            LogicNode::$STATE_ELSE
                        );

                        return;

                    // Close the logic branch.
                    case 'endif':
                        $stack->pop();

                        return;

                    default:
                        throw new ParserException(
                            printf(
                                'Unexpected token "%s". Expected "%s".', $words[0], implode(', ', ['else', 'endif'])
                            )
                        );
                }
            }
            elseif ($current instanceof LoopNode) {
                switch ($words[0]) {
                    case 'endfor':
                        $stack->pop();

                        return;

                    default:
                        throw new ParserException(
                            printf(
                                'Unexpected token "%s". Expected "%s".', $words[0], implode(', ', ['endfor'])
                            )
                        );
                }
            }
            else {
                // Detect unexpected single token expressions.
                throw new ParserException(
                    printf('Unexpected token "%s".', $words[0])
                );
            }
        }

        $node_types = $this->getLogicNodes();

        // It's a correct logic opening token.
        // Currently only if else and loops are supported.
        if (empty($node_types[$words[0]])) {
            throw new ParserException(
                printf('Unknown tag "%s" used, expected "%s".', $words[0], implode(', ', $this->getOpeningWords()))
            );
        }

        // Initialise a node.
        $node = new $node_types[$words[0]](substr($this->content, strlen($words[0])));

        // Append the node and make it a current one.
        $current->addChild($node);
        $stack->push($node);
    }

}
