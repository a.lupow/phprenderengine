<?php

namespace PRE\Tokens;

use PRE\Exceptions\ParserException;
use PRE\Nodes\ComponentNode;
use PRE\Stack;

class ComponentEndToken extends ComponentToken {

    public static function getTokenStart()
    {
        return '</pre-';
    }

    public function handle(Stack $stack)
    {
        // Validate current stack node.
        $node = $stack->current();
        if (!$node instanceof ComponentNode) {
            throw new ParserException('Unexpected component closing token.');
        }

        if ($node->getId() !== $this->content) {
            throw new ParserException(printf('Unexpected component closing tag, expected %s.', $node->getId()));
        }

        $stack->pop();
    }

}
