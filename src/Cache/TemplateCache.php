<?php

namespace PRE\Cache;

use PRE\CacheInterface;
use PRE\Environment;
use PRE\Template;

class TemplateCache implements CacheInterface {

    protected $env;
    protected $directory;
    protected $cache;

    public function __construct(Environment $environment, $directory)
    {
        $this->env = $environment;
        $this->directory = $directory;
        $this->cache = [];
    }

    protected function getClassPath($class) {
        return $this->directory . '/' . $class . '.php';
    }

    /**
     * {@inheritDoc}
     */
    public function getCache($filename)
    {
        $template = $this->env->loadTemplate($filename);

        // Check cache file existence.
        if (empty($this->cache[$filename])) {
            $path = $this->getClassPath($template->getCacheId());
            if (file_exists($path)) {
                $this->setCache($filename);
            }
        }

        return isset($this->cache[$filename]) ? $this->cache[$filename] : FALSE;
    }

    /**
     * {@inheritDoc}
     */
    public function setCache($filename, $content = NULL)
    {
        $template = $this->env->loadTemplate($filename);
        $class = $template->getCacheId();
        $path = $this->getClassPath($template->getCacheId());

        if (is_string($content)) {
            file_put_contents($path, $content);
        }

        return $this->cache[$template->getPath()] = [
            'class' => $class,
            'path' => $path,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function clearAll()
    {
        foreach ($this->cache as $item) {
            unlink($item['path']);
        }

        $this->cache = [];

        return $this;
    }

}
