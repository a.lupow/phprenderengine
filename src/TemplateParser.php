<?php

namespace PRE;

use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\AbstractNode;
use PHPHtmlParser\Dom\InnerNode;
use PRE\Blocks\ComponentBlock;
use PRE\Blocks\ExpressionBlock;
use PRE\Blocks\LogicBlock;
use PRE\Blocks\TextBlock;
use PRE\Blocks\WrapBlock;
use PRE\Exceptions\ParserException;
use PRE\Exceptions\PRException;
use PRE\Nodes\RootNode;
use PRE\Nodes\TextNode;
use PRE\Tokens\ComponentEndToken;
use PRE\Tokens\ComponentToken;
use PRE\Tokens\LogicToken;
use PRE\Tokens\OutputToken;

class TemplateParser implements ParserInterface {

    protected $env;

    protected $file;

    protected $char;

    /**
     * @var TokenBase[]
     */
    protected $tokens = [];

    protected $textBlock;

    protected $blockStack;

    public function __construct(Environment $environment)
    {
        $this->env = $environment;

        // Initialise the tokens.
        $this->tokens = [
            ComponentToken::class,
            ComponentEndToken::class,
            LogicToken::class,
            OutputToken::class,
        ];

        $this->textBlock = TextBlock::class;
    }

    public function __destruct()
    {
        $this->close();
    }

    public function close() {
        if (is_resource($this->file)) {
            fclose($this->file);
        }

        $this->file = null;
        $this->char = null;
        $this->blockStack = new Stack();
    }

    public function load($filepath) {
        $this->close();
        $this->file = fopen($filepath, 'r');
    }

    public function next() {
        return $this->char = fgetc($this->file);
    }

    public function current() {
        return $this->char;
    }

    public function isEOF() {
        return $this->char === FALSE;
    }

    public function expect(array $tokens) {
        $token_length = 0;
        array_walk($tokens, function ($element) use (&$token_length) {
            $token_length = max($token_length, strlen($element));
        });

        $content = '';
        $this->move(-1);

        for ($i = $token_length - 1; $i >= 0; $i--) {
            $content .= $this->next();

            if ($this->isEOF()) {
                break;
            }
        }

        // Move back.
        $this->move(-($token_length + ($i + 1) - 1));

        $found_token = FALSE;
        foreach ($tokens as $token) {
            if (strpos($content, $token) === 0) {
                $found_token = $token;

                break;
            }
        }

        if (!$found_token && $this->isEOF()) {
            throw new ParserException(printf('Unexpected end of the file. Expected tokens: %s.', join(', ', $tokens)));
        }

        return $found_token;
    }

    public function move($offset) {
        fseek($this->file, $offset, SEEK_CUR);

        return $this;
    }

    public function lookUp($token) {
        $token_size = strlen($token);
        $content = $this->char;

        if ($token_size > 1) {
            for ($i = $token_size - 2; $i >= 0; $i--) {
                $content .= $this->next();

                // EOF reached, skip the check.
                if ($this->isEOF()) {
                    fseek($this->file, -($token_size - 1 - $i), SEEK_END);
                    $token_size = 0;

                    break;
                }
            }

            fseek($this->file, -$token_size, SEEK_CUR);
            $this->next();
        }

        return $content === $token;
    }

    protected function detectToken() {
        foreach ($this->tokens as $token) {
            $token_start = $token::getTokenStart();

            if ($this->lookUp($token_start)) {
                // Offset the pointer on detected token.
                $this->move(strlen($token_start) - 1);

                return $token;
            }
        }

        return FALSE;
    }

    /**
     * Extracts semantic tokens from the target file.
     *
     * @return array
     * @throws ParserException]
     */
    public function tokenize() {
        $result = [];

        $expectations = new Stack();
        $buffer = '';

        while (!$this->isEOF()) {
            $char = $this->next();
            if ($char === FALSE) {
                break;
            }

            $expectation = $expectations->current();

            if (!$expectation && $token = $this->detectToken()) {
                // Store any buffer data as text.
                if ($buffer) {
                    $result[] = $buffer;
                    $buffer = '';
                }

                $expectations->push($token);
                continue;
            }

            if ($expectation) {
                $token_end = $expectation::getTokenEnd();

                // Expectation is reached, wrap up the token.
                if ($this->lookUp($token_end)) {
                    $expectations->pop();

                    $token = new $expectation();
                    $token->setContent($buffer);
                    $result[] = $token;

                    // Empty the buffer.
                    $buffer = '';

                    // Shift the pointer.
                    $this->move(
                        strlen($token_end) - 1
                    );
                }
                // Accumulate the buffer data.
                else {
                    $buffer .= $char;
                }
            }
            else {
                $buffer .= $char;
            }
        }

        // Append the ending buffer.
        if ($buffer) {
            $result[] = $buffer;
        }

        // If there are expectations left, trigger an exception.
        if ($expectations->count()) {
            throw new ParserException(
                printf(
                    'Unexpected end of file, expected "%s".', $expectations->current()::getTokenEnd()
                )
            );
        }

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function parse(Template $template): RootNode {
        $filepath = $template->getPath();
        $this->load($filepath);

        $tokens = $this->tokenize();
        $stack = new Stack();

        // Prepare a root node and push it to the stack.
        $root = new RootNode();
        $stack->push($root);

        foreach ($tokens as $token) {
            if ($token instanceof TokenBase) {
                $token->handle($stack);
            }
            else {
                $stack->current()->addChild(
                    new TextNode($token)
                );
            }
        }

        // Detect opened tags.
        if ($stack->count() > 1) {
            throw new ParserException('Unexpected end of the file.');
        }
        elseif (!$stack->current() instanceof RootNode) {
            throw new ParserException('Unknown error happened while parsing the template.');
        }

        return $root;
    }

}
