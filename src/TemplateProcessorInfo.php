<?php

namespace PRE;

class TemplateProcessorInfo implements TemplateProcessorInfoInterface {

    protected $class;
    protected $content;

    public function __construct($class, $content)
    {
        $this->class = $class;
        $this->content = $content;
    }

    /**
     * {@inheritDoc}
     */
    public function getTemplateClass(): string
    {
        return $this->class;
    }

    /**
     * {@inheritDoc}
     */
    public function getTemplateContent(): string
    {
        return $this->content;
    }

}
