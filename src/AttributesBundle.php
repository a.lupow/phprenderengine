<?php

namespace PRE;

class AttributesBundle {

    /**
     * An array of attributes.
     *
     * @var array[]
     */
    protected $attributes;

    /**
     * AttributesBundle constructor.
     *
     * @param $attributes
     */
    public function __construct($attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * Appends a class to the attribute bundle set.
     *
     * @param $class
     *   A class.
     *
     * @return $this
     */
    public function addClass($class) {
        if (is_array($class)) {
            foreach ($class as $item) {
                $this->addClass($item);
            }

            return $this;
        }

        if (!isset($this->attributes['class'])) {
            $this->attributes['class'] = [];
        }

        if (!in_array($class, $this->attributes['class'])) {
            $this->attributes['class'][] = $class;
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {
        $attributes = [];

        foreach ($this->attributes as $attribute => $value) {
            // Class attribute special case.
            if ($attribute === 'class') {
                $value = implode(' ', $value);
            }
            // Special case for non scalar values.
            elseif (!is_scalar($value)) {
                $value = json_encode($value);
                $attributes[] = "$attribute=$value";

                continue;
            }

            $attributes[] = "$attribute=\"$value\"";
        }

        return implode(' ', $attributes);
    }

}
