<?php

namespace PRE;

class Stack implements \Countable {

    protected $data;
    protected $pointer;

    public function __construct()
    {
        $this->pointer = -1;
        $this->data = [];
    }

    public function current() {
        if (!isset($this->data[$this->pointer])) {
            return FALSE;
        }

        return $this->data[$this->pointer];
    }

    public function push($data) {
        $this->data[++$this->pointer] = $data;

        return $data;
    }

    public function pop() {
        $result = $this->data[$this->pointer];
        unset($this->data[$this->pointer]);

        $this->pointer--;

        return $result;
    }

    public function empty() {
        $this->data = [];
        $this->pointer = 0;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function count()
    {
        return count($this->data);
    }

}
