<?php

namespace PRE;

interface CompilerInterface {

    /**
     * Returns current compiler environment.
     *
     * @return Environment
     */
    public function getEnvironment(): Environment;

    /**
     * Appends compiler source line.
     *
     * @param string $line
     *   A line to be appended.
     *
     * @return $this
     */
    public function addLine($line);

    /**
     * Assigns an expression to a buffer.
     *
     * @param string $expression
     *   Expression to be assigned.
     * @param int $buffer
     *   Optional buffer index.
     *
     * @return $this
     */
    public function assignToBuffer($expression, $buffer = NULL);

    /**
     * Concats an expression with a buffer.
     *
     * @param $expression
     *   Expression to be concatinated.
     * @param int $buffer
     *   Optional buffer index.
     *
     * @return mixed
     */
    public function concatBuffer($expression, $buffer = NULL);

    /**
     * Creates a new buffer for expressions to be stored.
     *
     * @return $this
     */
    public function fork();

    /**
     * Returns to previous buffer.
     *
     * @return $this
     */
    public function unfork();

    /**
     * Returns current buffer.
     *
     * @return integer
     */
    public function getBuffer();

    /**
     * Processes an input filename into a class file.
     *
     * @param Template $template
     *   Target template.
     *
     * @return TemplateProcessorInfoInterface
     *   Template processing result.
     */
    public function compile(Template $template): TemplateProcessorInfoInterface;

}
