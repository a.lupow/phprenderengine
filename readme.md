PhpRenderEngine
================
This project aims to provide a twig-like (with partial compatibility) template engine, 
providing a way to utilise reusable assets (called components). The components are completely 
separable from each other, containing both templates and frontend resources (such as js and css) 
required for them to function correctly.

Components
-----------
Components are being descovered by component loaders. By default, the engine provides a simple 
_file loader_, allowing users to specify a list of root namespaces to look at.

Normally, users would store each component in a separate directory. Example: component `foo` must 
be stored in a separate `foo` directory, containing a `FooComponent.php` class file, defining the component 
with a `FooComponent` class.

Templates
---------
Templates can specify either simple html text data, logical blocks (`{%%}`) or output statements (`{{}}`). 
Components are to be described as any other html element, except with a `pre-` prefix. Example: 
`<pre-foo>bar</pre-foo>` will render out a `FooComponent` component class with `bar` content string passed 
to it. Components accept data binding, described with output statements used instead of a simple quote mark.

Project state
--------
The project is currently at its earlier stage, supporting only _if_, _else_ and _for_ logic statements.
Currently, there is only a yii2 full integration available, planning to introduce laravel and drupal 9
in the future.
